This is an example UDP server.

The scripts are designed to run under linux.  You can easily modify
them to run in Windows... I didn't use anything like ant or maven
to make it easier to build and run and to not obfuscate what it
is doing.

To build the server, go to the root directery of this repository
and type ./build.sh

To run the demo, open a window.  Go to the root of this repository
and type ./runServer.sh

Open another window.  Go to the root of the repository and type 
./runClient.sh

You should see the server do a hex dump of the message and call the
data access object to save the parsed location.


