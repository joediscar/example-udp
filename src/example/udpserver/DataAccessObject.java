package example.udpserver;

import java.util.List;

import utils.locations.models.Location;
import utils.locations.models.ParsedLocation;

/**
 * This is just a stub class to demonstrate saving data.  Really it ought to be a hibernate/JPA
 * solution, but this way is easier to understand
 * 
 * @author discar
 *
 */
public class DataAccessObject {
  

  public void create(ParsedLocation data) {
    if (retrieve(data.getDeviceId()) != null) {
      // Record already exists.  He probably
      // meant "update"
      update(data);
      return;
    }
    
    // TODO: insert a new record here
    // This is how to get the location from the parsed Data.   The data contains a list of
    // Locations.  The LAST location is the most recent MAJOR location.  All other Locations
    // are "minors" that contain only the position and speed.
    List<Location> locations = data.getLocations();
    Location lastLocation = locations.get(locations.size()-1);
    
    // Locations are represented by integers.  To obtain the real lat/long, you must
    // divide by 10^7
    double lat = ((double) lastLocation.getLat() / 10000000.0);
    double lng = ((double) lastLocation.getLng() / 10000000.0);
    
    System.out.printf("*** Inserting new record: %s   (%f, %f)\n", data.getDeviceId(), lat, lng);
  }
  
  public ParsedLocation retrieve(String deviceId) {
    ParsedLocation data = null;
    
    // TODO: Retrieve data here using the string in deviceId
    // NOTE: the deviceId in most of our implementations
    // include FJ20xx is the IMEI of the device in 
    // string form.
    //
    return data;
  }
  
  public void update(ParsedLocation data) {
    if (data.getDeviceId() == null) {
      // Nothing to do
      return;
    }
    
    // TODO: Update the record in the database here using
    // the string returned by getDeviceId() as the key
    //
    
  }
}
