package example.udpserver;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import parser.MoaParser;
import utils.Utility;
import utils.api.models.SerialPacket;
import utils.locations.models.ParsedLocation;
import utils.logs.ConsoleLogger;

/**
 * This is an example fully functioning UDP device server.   It receives a packet from UDP
 * and simulates writing the parsed data to a database.
 * 
 * @author discar
 *
 */
public class Server {

   static int PORT = 10000;
   static int MAXMTU = 1500;
   
   public static void main(String args[]) {
     for (int i=0; i<args.length; ++i) {
       if (args[i].equalsIgnoreCase("-p")) {
         PORT = Integer.parseInt(args[++i]);
       } else {
           System.out.println("-p {port}      Sets UDP port for server currently "+PORT);
           System.exit(1);
       }
     }
     Server server = new Server();
     server.start();
   }
     
   /**
    * Runs the server
    */
   public void start() {
     try (
         // The serversocket we listen to
         DatagramSocket serverSocket = new DatagramSocket(PORT);
     ) {

       
       /*
        * Loop forever listening on UDP port 
        */
       while(true) {
         // Recieve Buffer
         // We place them inside this loop so each datagram packet gets its own buffer.  If we 
         // don't do this way, you must take care that the data is processed before you receive
         // another one
         byte[] receiveData = new byte[MAXMTU];

         final DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length );
         serverSocket.receive(receivePacket);
         
         // When the receive() returns, the data is in receivePacket.  Here's we'll launch a thread to 
         // handle the processing.  But you might want to look at the Executor model 
         // as a more robust implementation: https://docs.oracle.com/javase/tutorial/essential/concurrency/executors.html
         //
         // For example, our example implementation doesn't take into account that we probably want to 
         // limit how many threads start.
         Thread t = new Thread() {
           public void run() {
             try {
               byte data[] = receivePacket.getData();
               int length = receivePacket.getLength();
               
               // Data length could be wrong from the receivePacket, truncate off erroneous bytes
               byte justData[] = new byte[length];
               for (int i=0; i<length; ++i) {
                 justData[i] = data[i];
               }
               data = justData;
               

               // Parse the MOA message
               MoaParser parser = new MoaParser(null, new ConsoleLogger());
               
               // For historical reasons, the data must be in a Serial Packet object
               SerialPacket serialPacket = new SerialPacket();
               serialPacket.setData(data);
               Utility.hexDump(serialPacket.getData(), length);
               ParsedLocation parsedLocation = parser.handleLocationWithFletch(false, serialPacket);
               
               // The parsedLocation object contains the ack packet we want to send
               // back to the device
               byte[] ackData = parsedLocation.getAckData();
               if (ackData != null && ackData.length > 0) {
                 // There is something we need to ack
                 
                 // 
                 // The ack gets sent back to where we got the message from
                 int returnPort = receivePacket.getPort();    // This is the port we need to ack to
                 InetAddress ipAddress = receivePacket.getAddress(); // This is the ip address
                 

                 DatagramPacket sendPacket = new DatagramPacket(ackData, ackData.length, ipAddress, returnPort);
                 serverSocket.send(sendPacket);
            
               }
               
               
               // This example does data access using the old model--it's easier to understand.  In reality
               // the server should use something like Hibernate/JPA to persist the parsed location to 
               // the database.
               DataAccessObject dao = new DataAccessObject();
               dao.create(parsedLocation);
               
             } catch (Exception ex) {
                 ex.printStackTrace();
             }
           }
         };
         t.start();
         
       }
       
     } catch (Exception ex) {
       ex.printStackTrace();
     }
   }
}
